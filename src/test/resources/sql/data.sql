USE dwfe_test;

LOCK TABLES
dwfe_countries WRITE,
dwfe_nevis_authorities WRITE,
dwfe_nevis_account_access WRITE,
dwfe_nevis_account_email WRITE,
dwfe_nevis_account_phone WRITE,
dwfe_nevis_account_personal WRITE,
dwfe_nevis_account_authority WRITE;

--
-- DWFE
--

INSERT INTO dwfe_countries
VALUES ('Russia', 'RU', 'RUS', '7'),
       ('Ukraine', 'UA', 'UKR', '380'),
       ('Germany', 'DE', 'DEU', '49'),
       ('United States', 'US', 'USA', '1'),
       ('United Kingdom', 'GB', 'GBR', '44'),
       ('Japan', 'JP', 'JPN', '81');

--
-- MODULE: Nevis
--
-- Password for account email 'test1@dwfe.ru' = test11
-- Password for account email 'test2@dwfe.ru' = test22
--

INSERT INTO dwfe_nevis_authorities
VALUES ('NEVIS_ADMIN', 'Administrator of Nevis'),
       ('NEVIS_USER', 'Standard Nevis User'),
       ('BALI_ADMIN', 'Administrator of Bali'),
       ('BALI_USER', 'Standard Bali User'),
       ('STORAGE_ADMIN', 'Administrator of Storage'),
       ('STORAGE_USER', 'Standard Storage User');

INSERT INTO dwfe_nevis_account_access
VALUES (1000, '{bcrypt}$2a$10$cWUX5MiFl8rJFVxKxEbON.2QcJ/0RsVfhVvvqDG5wEOM/bstMIk6m', null, null, 1, 1, 1, 1, '2017-07-07 07:07:07', '2017-07-07 07:07:07'),
       (1001, '{bcrypt}$2a$10$9SCLBifjy2Ieaoc6VLmSgOQsxf4NUlbGO32zMraftTXcl3jEAqlbm', null, 'https://assets.gitlab-static.net/uploads/-/system/user/avatar/2466042/avatar.png?width=160', 1, 1, 1, 1, '2017-07-07 07:07:07', '2017-07-07 07:07:07');

INSERT INTO dwfe_nevis_account_authority
VALUES (1000, 'NEVIS_ADMIN'),
       (1000, 'NEVIS_USER'),
       (1000, 'BALI_USER'),
       (1000, 'STORAGE_USER'),
       (1001, 'NEVIS_USER'),
       (1001, 'BALI_USER'),
       (1001, 'STORAGE_USER');

INSERT INTO dwfe_nevis_account_email
VALUES (1000, 'test1@dwfe.ru', 0, 1, '2017-07-07 07:07:07'),
       (1001, 'test2@dwfe.ru', 1, 1, '2017-07-07 07:07:07');

INSERT INTO dwfe_nevis_account_phone
VALUES (1000, '+79990011273', 0, 1, '2017-07-07 07:07:07'),
       (1001, '+79094141719', 1, 1, '2017-07-07 07:07:07');

INSERT INTO dwfe_nevis_account_personal
VALUES (1000, 'test1', 0, null, 0, null, 0, null, 0, null, 0, null, 0, null, 0, null, 0, null, 0, null, 0, '2017-07-07 07:07:07'),
       (1001, 'test2', 1, null, 1, null, 1, null, 1, null, 1, null, 1, null, 1, null, 1, null, 1, null, 1, '2017-07-07 07:07:07');


UNLOCK TABLES;