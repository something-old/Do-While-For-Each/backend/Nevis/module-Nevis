package dwfe.test.config;

public enum DwfeTestAccountUsernameType
{
  EMAIL,
  PHONE,
  NICKNAME,
  ID,
}
