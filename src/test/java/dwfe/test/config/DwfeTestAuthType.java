package dwfe.test.config;

public enum DwfeTestAuthType
{
  SIGN_IN,
  REFRESH
}
