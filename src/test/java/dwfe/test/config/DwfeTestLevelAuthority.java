package dwfe.test.config;

public enum DwfeTestLevelAuthority
{
  ANY,
  USER,
  ADMIN
}
