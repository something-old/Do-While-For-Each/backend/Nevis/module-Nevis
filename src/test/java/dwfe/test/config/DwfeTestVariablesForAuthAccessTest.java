package dwfe.test.config;

import org.springframework.web.bind.annotation.RequestMethod;

import java.util.Map;

import static dwfe.test.config.DwfeTestLevelAuthority.*;

public abstract class DwfeTestVariablesForAuthAccessTest
{
  public abstract Map<String, Map<DwfeTestLevelAuthority, Map<RequestMethod, Map<String, Object>>>> RESOURCE_AUTHORITY_reqDATA();

  //-------------------------------------------------------
  // Expected statuses:
  //    200 = OK
  //    400 = Bad Request
  //    401 = Unauthorized
  //    403 = Forbidden, access_denied

  public static final Map<DwfeTestLevelAuthority, Map<DwfeTestLevelAuthority, Integer>> AUTHORITY_to_AUTHORITY_STATUS = Map.of(
          ANY, Map.of(
                  ANY, 200,
                  USER, 401,
                  ADMIN, 401),
          USER, Map.of(
                  ANY, 200,
                  USER, 200,
                  ADMIN, 403),
          ADMIN, Map.of(
                  ANY, 200,
                  USER, 200,
                  ADMIN, 200)
  );

  public static final Map<DwfeTestLevelAuthority, Map<DwfeTestLevelAuthority, Integer>> AUTHORITY_to_AUTHORITY_STATUS_BAD_ACCESS_TOKEN = Map.of(
          USER, Map.of(
                  ANY, 401,
                  USER, 401,
                  ADMIN, 401),
          ADMIN, Map.of(
                  ANY, 401,
                  USER, 401,
                  ADMIN, 401)
  );
}
