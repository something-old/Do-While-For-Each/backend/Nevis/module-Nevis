package dwfe.modules.nevis;

import dwfe.config.DwfeConfigProperties;
import dwfe.modules.nevis.config.NevisConfigProperties;
import dwfe.modules.nevis.db.account.access.NevisAccountAccess;
import dwfe.modules.nevis.db.account.access.NevisAccountAccessService;
import dwfe.modules.nevis.db.account.access.NevisAccountThirdParty;
import dwfe.modules.nevis.db.account.access.NevisAccountUsernameType;
import dwfe.modules.nevis.db.account.email.NevisAccountEmail;
import dwfe.modules.nevis.db.account.email.NevisAccountEmailService;
import dwfe.modules.nevis.db.account.personal.NevisAccountPersonal;
import dwfe.modules.nevis.db.account.personal.NevisAccountPersonalService;
import dwfe.modules.nevis.db.account.phone.NevisAccountPhone;
import dwfe.modules.nevis.db.account.phone.NevisAccountPhoneService;
import dwfe.modules.nevis.db.mailing.NevisMailing;
import dwfe.modules.nevis.db.mailing.NevisMailingService;
import dwfe.modules.nevis.db.mailing.NevisMailingType;
import dwfe.test.DwfeTestAuth;
import dwfe.test.DwfeTestChecker;
import dwfe.test.DwfeTestClient;
import dwfe.test.DwfeTestUtil;
import dwfe.test.config.DwfeTestAccountUsernameType;
import dwfe.test.config.DwfeTestConfigProperties;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDate;
import java.util.*;
import java.util.concurrent.TimeUnit;

import static dwfe.db.other.DwfeGender.M;
import static dwfe.modules.nevis.NevisTestVariablesForApiTest.*;
import static dwfe.modules.nevis.db.account.access.NevisAccountThirdParty.GOOGLE;
import static dwfe.modules.nevis.db.mailing.NevisMailingType.*;
import static dwfe.modules.nevis.util.NevisUtil.*;
import static dwfe.test.DwfeTestUtil.pleaseWait;
import static dwfe.test.config.DwfeTestAccountUsernameType.*;
import static dwfe.test.config.DwfeTestAuthType.REFRESH;
import static dwfe.test.config.DwfeTestAuthType.SIGN_IN;
import static dwfe.test.config.DwfeTestLevelAuthority.USER;
import static dwfe.test.config.DwfeTestTypeResourceAccessing.USUAL;
import static org.junit.Assert.*;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

//
// == https://spring.io/guides/gs/testing-web/
//

@RunWith(SpringRunner.class)
@SpringBootTest(
        webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT  // == https://docs.spring.io/spring-boot/docs/current/reference/html/boot-features-testing.html#boot-features-testing-spring-boot-applications
)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class NevisTest
{
  private static final Logger log = LoggerFactory.getLogger(NevisTest.class);
  private static Set<String> auth_test_access_tokens = new HashSet<>();

  @Autowired
  private NevisConfigProperties propNevis;
  @Autowired
  private DwfeConfigProperties propDwfe;
  @Autowired
  private DwfeTestConfigProperties propDwfeTest;
  @Autowired
  private DwfeTestUtil utilTest;
  @Autowired
  private DwfeTestAuth auth;
  @Autowired
  private DwfeTestClient client;
  @Autowired
  private NevisAccountAccessService accessService;
  @Autowired
  private NevisAccountEmailService emailService;
  @Autowired
  private NevisAccountPhoneService phoneService;
  @Autowired
  private NevisAccountPersonalService personalService;
  @Autowired
  private NevisMailingService mailingService;


  //-------------------------------------------------------
  // ACTUATOR
  //

  @Test
  public void _00_01_actuator()
  {
    pleaseWait(85, log);
    utilTest.test_actuator();
  }


  //-------------------------------------------------------
  // Auth
  //

  @Test
  public void _01_01_USER() throws InterruptedException
  {
    var user = auth.getUSER();
    auth_test_access_tokens.add(user.access_token);
    fullAuth(user);
  }

  @Test
  public void _01_02_ADMIN()
  {
    var admin = auth.getADMIN();
    auth_test_access_tokens.add(admin.access_token);
    fullAuth(admin);
  }

  @Test
  public void _01_03_ANY()
  {
    var anonymous = auth.getAnonymous();
    utilTest.resourceAccessingProcess(anonymous.access_token, anonymous.levelAuthority, USUAL);
  }

  @Test
  public void _01_04_different_access_tokens()
  {
    log.info("\n\n{}", String.join("\n", auth_test_access_tokens));
    assertEquals(2, auth_test_access_tokens.size());
  }

  @Test
  public void _01_05_signIn()
  {
    var clientTrusted = client.getClientTrusted();

    // EMAIL, usernameType=null
    var signInResp = utilTest.tokenProcess(SIGN_IN, auth.of(Account2_EMAIL, null, Account2_Pass, clientTrusted), 200);
    minTokenEnhanceDataCheck(signInResp);
    utilTest.tokenProcess(SIGN_IN, auth.of(Account2_EMAIL + "1", null, Account2_Pass, clientTrusted), 400);
    // NICKNAME, usernameType=null
    utilTest.tokenProcess(SIGN_IN, auth.of(Account2_NICKNAME, null, Account2_Pass, clientTrusted), 200);
    utilTest.tokenProcess(SIGN_IN, auth.of(Account2_NICKNAME + "1", null, Account2_Pass, clientTrusted), 400);
    // PHONE, usernameType=null
    utilTest.tokenProcess(SIGN_IN, auth.of(Account2_PHONE, null, Account2_Pass, clientTrusted), 200);
    utilTest.tokenProcess(SIGN_IN, auth.of(Account2_PHONE + "1", null, Account2_Pass, clientTrusted), 400);
    // ID, usernameType=null
    utilTest.tokenProcess(SIGN_IN, auth.of(Account2_ID, null, Account2_Pass, clientTrusted), 200);
    utilTest.tokenProcess(SIGN_IN, auth.of(Account2_ID + "1", null, Account2_Pass, clientTrusted), 400);

    // EMAIL, usernameType=EMAIL
    utilTest.tokenProcess(SIGN_IN, auth.of(Account2_EMAIL, EMAIL, Account2_Pass, clientTrusted), 200);
    utilTest.tokenProcess(SIGN_IN, auth.of(Account2_EMAIL + "1", EMAIL, Account2_Pass, clientTrusted), 400);
    // NICKNAME, usernameType=NICKNAME
    utilTest.tokenProcess(SIGN_IN, auth.of(Account2_NICKNAME, NICKNAME, Account2_Pass, clientTrusted), 200);
    utilTest.tokenProcess(SIGN_IN, auth.of(Account2_NICKNAME + "1", NICKNAME, Account2_Pass, clientTrusted), 400);
    // PHONE, usernameType=PHONE
    utilTest.tokenProcess(SIGN_IN, auth.of(Account2_PHONE, PHONE, Account2_Pass, clientTrusted), 200);
    utilTest.tokenProcess(SIGN_IN, auth.of(Account2_PHONE + "1", PHONE, Account2_Pass, clientTrusted), 400);
    // ID, usernameType=ID
    utilTest.tokenProcess(SIGN_IN, auth.of(Account2_ID, ID, Account2_Pass, clientTrusted), 200);
    utilTest.tokenProcess(SIGN_IN, auth.of(Account2_ID + "1", ID, Account2_Pass, clientTrusted), 400);
  }

  @Test
  public void _01_06_tokenRefresh()
  {
    var clientTrusted = client.getClientTrusted();
    var auth0 = auth.of(Account1_EMAIL, EMAIL, Account1_Pass, clientTrusted);

    var signInResp = utilTest.tokenProcess(SIGN_IN, auth0, 200);
    var tokenRefreshResp = utilTest.tokenProcess(REFRESH, auth0, 200);
    minTokenEnhanceDataCheck(tokenRefreshResp);
  }


  //-------------------------------------------------------
  // Account.Common
  //

  @Test
  public void _02_01_canUseUsername()
  {
    var resource = propNevis.getResource().getCanUseUsername();
    utilTest.check(nevisApiRoot() + resource, POST, auth.getAnonym_accessToken(), checkers_for_canUseUsername);
  }

  @Test
  public void _02_02_canUsePassword()
  {
    var resource = propNevis.getResource().getCanUsePassword();
    utilTest.check(nevisApiRoot() + resource, POST, auth.getAnonym_accessToken(), checkers_for_canUsePassword);
  }

  @Test
  public void _02_03_createAccount()
  {
    var resource = propNevis.getResource().getCreateAccount();
    utilTest.check(nevisApiRoot() + resource, POST, auth.getAnonym_accessToken(), checkers_for_createAccount());
    //
    // Was created 5 new accounts:
    //  - Account3 - EMAIL, password was not passed
    //  - Account4 - NICKNAME, password was passed
    //  - Account5 - PHONE, already encoded password was passed
    //  - Account6 - EMAIL, password was passed + Third-party
    //  - Account7 - EMAIL, values for check restrictions


    // Account3
    var aAccess1 = checkAccountAccessAfterCreateAccount(Account3_EMAIL, EMAIL, null);
    assertNull(aAccess1.getAvatarUrl());
    var id = aAccess1.getId();
    var aEmail = getAccountEmailById(id, true);
    checkAccountEmail(aEmail, Account3_EMAIL, true, true);
    getAccountPhoneById(id, false);
    checkAccountPersonal_ExactMatch(id, NevisAccountPersonal.of(
            null, true,       // nickName
            "ozon", true,     // firstName
            "Alice", true,    // middleName
            "sunshine", true, // lastName
            null, true,       // gender
            null, true,       // dateOfBirth
            null, true,       // country
            null, true,       // city
            null, true,       // company
            null, true)       // positionHeld
    );
    var mailing = getMailingFirstOfOne(WELCOME_PASSWORD, aEmail.getValue());
    var password = mailing.getData();
    assertTrue(password.length() >= 9);
    Account3_Pass = password; // for next tests


    // Account4
    var aAccess2 = checkAccountAccessAfterCreateAccount(Account4_NICKNAME, NICKNAME, null);
    assertNull(aAccess2.getAvatarUrl());
    id = aAccess2.getId();
    Account4_ID = id;
    getAccountEmailById(id, false);
    getAccountPhoneById(id, false);
    checkAccountPersonal_ExactMatch(id, NevisAccountPersonal.of(
            Account4_NICKNAME, true,              // nickName
            null, true,                           // firstName
            null, true,                           // middleName
            null, true,                           // lastName
            M, true,                              // gender
            LocalDate.parse("1980-11-27"), true,  // dateOfBirth
            "DE", true,                           // country
            null, true,                           // city
            null, true,                           // company
            null, true)                           // positionHeld
    );
    var mailingList = mailingService.findByEmail(Account4_EMAIL);
    assertEquals(0, mailingList.size());


    // Account5
    var aAccess3 = checkAccountAccessAfterCreateAccount(Account5_PHONE, PHONE, null);
    assertEquals("http://localhost:4200/avatar.png", aAccess3.getAvatarUrl());
    id = aAccess3.getId();
    Account5_ID = id;
    getAccountEmailById(id, false);
    var aPhone = getAccountPhoneById(id, true);
    checkAccountPhone(aPhone, Account5_PHONE, true, false);
    checkAccountPersonal_ExactMatch(id, NevisAccountPersonal.of(
            null, true,           // nickName
            null, true,           // firstName
            null, true,           // middleName
            null, true,           // lastName
            null, true,           // gender
            null, true,           // dateOfBirth
            null, true,           // country
            "Dallas", true,       // city
            "Home Ltd.", true,    // company
            "programmer", true)   // positionHeld
    );
    mailingList = mailingService.findByEmail(Account5_EMAIL);
    assertEquals(0, mailingList.size());


    // Account6
    var aAccess4 = checkAccountAccessAfterCreateAccount(Account6_EMAIL, EMAIL, GOOGLE);
    assertNull(aAccess4.getAvatarUrl());
    id = aAccess4.getId();
    Account6_ID = id;
    aEmail = getAccountEmailById(id, true);
    checkAccountEmail(aEmail, Account6_EMAIL, true, false);
    getAccountPhoneById(id, false);
    checkAccountPersonal_ExactMatch(id, NevisAccountPersonal.of(
            null, true, // nickName
            null, true, // firstName
            null, true, // middleName
            null, true, // lastName
            null, true, // gender
            null, true, // dateOfBirth
            null, true, // country
            null, true, // city
            null, true, // company
            null, true) // positionHeld
    );
    mailing = getMailingFirstOfOne(WELCOME_ONLY, aEmail.getValue());
    assertEquals(0, mailing.getData().length());


    // Account7
    var aAccess5 = checkAccountAccessAfterCreateAccount(Account7_EMAIL, EMAIL, null);
    assertNull(aAccess5.getAvatarUrl());
    id = aAccess5.getId();
    Account7_ID = id;
    aEmail = getAccountEmailById(id, true);
    checkAccountEmail(aEmail, Account7_EMAIL, true, false);
    aPhone = getAccountPhoneById(id, true);
    checkAccountPhone(aPhone, "1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890", true, false);
    checkAccountPersonal_ExactMatch(id, NevisAccountPersonal.of(
            "1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890", true, // nickName
            "12345678901234567890", true, // firstName
            "12345678901234567890", true, // middleName
            "12345678901234567890", true, // lastName
            null, true,                   // gender
            null, true,                   // dateOfBirth
            null, true,                   // country
            "1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890", true, // city
            "1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890", true, // company
            "1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890", true) // positionHeld
    );
    mailing = getMailingFirstOfOne(WELCOME_ONLY, aEmail.getValue());
    assertEquals(0, mailing.getData().length());


    // perform full auth test for newly created accounts
    var clientTrusted = client.getClientTrusted();
    fullAuth(auth.of(USER, aAccess1.getSignedInUsername(), nevisUsernameTypeToDwfe(aAccess1.getUsernameType()), Account3_Pass, clientTrusted));
    fullAuth(auth.of(USER, aAccess2.getSignedInUsername(), nevisUsernameTypeToDwfe(aAccess2.getUsernameType()), Account4_Pass, clientTrusted));
//    fullAuth(auth.of(USER, aAccess3.getSignedInUsername(), nevisUsernameTypeToDwfe(aAccess3.getUsernameType()), Account5_Pass, clientTrusted));
    fullAuth(auth.of(USER, aAccess4.getSignedInUsername(), nevisUsernameTypeToDwfe(aAccess4.getUsernameType()), Account6_Pass, clientTrusted));
    fullAuth(auth.of(USER, aAccess5.getSignedInUsername(), nevisUsernameTypeToDwfe(aAccess5.getUsernameType()), Account7_Pass_Decoded, clientTrusted));
  }

  @Test
  public void _02_04_id()
  {
    var resource = "/id/";
    utilTest.check(nevisApiRoot() + resource + "1", GET, auth.getAnonym_accessToken(), checkers_for_id_error);
    utilTest.check(nevisApiRoot() + resource + Account1_ID, GET, auth.getAnonym_accessToken(), checkers_for_id_1000);
    utilTest.check(nevisApiRoot() + resource + Account2_ID, GET, auth.getAnonym_accessToken(), checkers_for_id_1001);
    utilTest.check(nevisApiRoot() + resource + Account5_ID, GET, auth.getAnonym_accessToken(), checkers_for_id(Account5_ID));
  }

  @Test
  public void _02_05_thirdPartyAuth()
  {
    var resource = propNevis.getResource().getThirdPartyAuth();
    utilTest.check(nevisApiRoot() + resource, POST, auth.getAnonym_accessToken(), checkers_for_thirdPartyAuth);
  }

  @Test
  public void _99_01_deleteAccount()
  {
    var auth1 = auth.of(USER, Account1_EMAIL, EMAIL, Account1_Pass, client.getClientTrusted());
    var resource = propNevis.getResource().getDeleteAccount();

    utilTest.check(nevisApiRoot() + resource, POST, auth1.access_token, checkers_for_deleteAccount);

    assertFalse(accessService.findById(Long.parseLong(Account1_ID)).isPresent());
    assertFalse(emailService.findById(Long.parseLong(Account1_ID)).isPresent());
    assertFalse(phoneService.findById(Long.parseLong(Account1_ID)).isPresent());
    assertFalse(personalService.findById(Long.parseLong(Account1_ID)).isPresent());
  }


  //-------------------------------------------------------
  // Account.Access
  //

  @Test
  public void _03_01_getAccountAccess()
  {
    var resource = propNevis.getResource().getGetAccountAccess();
    utilTest.check(nevisApiRoot() + resource, GET, auth.getADMIN().access_token, checkers_for_getAccountAccess1);

    var auth1 = auth.of(USER, Account6_EMAIL, EMAIL, Account6_Pass, client.getClientTrusted());
    utilTest.check(nevisApiRoot() + resource, GET, auth1.access_token, checkers_for_getAccountAccess2(Account6_ID));
  }

  @Test
  public void _03_02_passwordChange()
  {
    passwordChange(Account6_EMAIL, EMAIL, Account6_Pass, Account6_NewPass, checkers_for_passwordChange);
    passwordChange(Account7_EMAIL, EMAIL, Account7_Pass_Decoded, Account7_NewPass_Decoded, checkers_for_passwordChange_2);
  }

  private void passwordChange(String username, DwfeTestAccountUsernameType usernameType, String curpass, String newpass, List<DwfeTestChecker> checkers)
  {
    var clientTrusted = client.getClientTrusted();
    var resource = propNevis.getResource().getPasswordChange();

    var auth1 = auth.of(username, usernameType, newpass, clientTrusted);
    utilTest.tokenProcess(SIGN_IN, auth1, 400);
    auth1.password = curpass;
    utilTest.tokenProcess(SIGN_IN, auth1, 200);

    utilTest.check(nevisApiRoot() + resource, POST, auth1.access_token, checkers);

    utilTest.tokenProcess(SIGN_IN, auth1, 400);
    auth1.password = newpass;
    utilTest.tokenProcess(SIGN_IN, auth1, 200);

    fullAuth(auth1);
  }

  @Test
  public void _03_03_passwordReset()
  {
    performPasswordReset(Account6_EMAIL, EMAIL, Account6_NewPass, Account6_Pass, Account6_Pass);
    performPasswordReset(Account7_EMAIL, EMAIL, Account7_NewPass_Decoded, Account7_Pass_Decoded, Account7_Pass_Encoded);
  }

  private void performPasswordReset(String username, DwfeTestAccountUsernameType usernameType, String curpass, String newpass, String newpassForCheckers)
  {
    passwordResetReq(username);
    passwordReset(username, usernameType, curpass, newpass, newpassForCheckers);
  }

  private void passwordResetReq(String username)
  {
    var resource = propNevis.getResource().getPasswordResetReq();
    var timeToWait = TimeUnit.MILLISECONDS.toSeconds(propNevis.getScheduledTaskMailing().getCollectFromDbInterval()) * 3;
    var type = PASSWORD_RESET_CONFIRM;

    assertEquals(0, mailingService.findByTypeAndEmail(type, username).size());

    utilTest.check(nevisApiRoot() + resource, POST, null, checkers_for_passwordResetReq(username));
    utilTest.check(nevisApiRoot() + resource, POST, null, checkers_for_passwordResetReq_duplicateDelay(username));

    pleaseWait(timeToWait, log);
    var letter = getMailingFirstOfOne(type, username);
    assertFalse(letter.isMaxAttemptsReached());
    assertTrue(letter.getData().length() >= 28);

    utilTest.check(nevisApiRoot() + resource, POST, null, checkers_for_passwordResetReq(username));

    pleaseWait(timeToWait, log);
    assertEquals(2, mailingService.findScheduledNotEmptyData(type, username).size());
  }

  private void passwordReset(String username, DwfeTestAccountUsernameType usernameType, String curpass, String newpass, String newpassForCheckers)
  {
    var type = PASSWORD_RESET_CONFIRM;
    var clientTrusted = client.getClientTrusted();
    var resource = propNevis.getResource().getPasswordReset();

    var mailingList = mailingService.findScheduledNotEmptyData(type, username);
    assertEquals(2, mailingList.size());

    var auth1 = auth.of(username, usernameType, newpass, clientTrusted);
    utilTest.tokenProcess(SIGN_IN, auth1, 400);
    auth1.password = curpass;
    utilTest.tokenProcess(SIGN_IN, auth1, 200);

    //change password
    utilTest.check(nevisApiRoot() + resource, POST, null,
            checkers_for_passwordReset(username, newpassForCheckers, mailingList.get(0).getData()));

    assertEquals(1, mailingService.findScheduledNotEmptyData(type, username).size());

    utilTest.tokenProcess(SIGN_IN, auth1, 400);
    auth1.password = newpass;
    utilTest.tokenProcess(SIGN_IN, auth1, 200);

    fullAuth(auth1);
  }

  @Test
  public void _03_04_setAvatarUrl()
  {
    var resource = propNevis.getResource().getSetAvatarUrl();
    var clientTrusted = client.getClientTrusted();
    var dataKey = "data";
    var avatarUrlKey = "avatarUrl";

    var auth0 = auth.of(USER, Account5_PHONE, PHONE, Account5_Pass, clientTrusted);

    var access0 = getAccessByUsername(Account5_PHONE, NevisAccountUsernameType.PHONE);
    var avatarUrlValue = access0.getAvatarUrl();
    var signInResp = utilTest.tokenProcess(SIGN_IN, auth0, 200);
    minTokenEnhanceDataCheck(signInResp);
    var data = (Map<String, Object>) signInResp.get(dataKey);
    assertNotNull(avatarUrlValue);
    assertEquals(avatarUrlValue, data.get(avatarUrlKey));

    utilTest.check(nevisApiRoot() + resource, POST, auth0.access_token, checkers_for_setAvatarUrl1);
    access0 = getAccessByUsername(Account5_PHONE, NevisAccountUsernameType.PHONE);
    avatarUrlValue = access0.getAvatarUrl();
    utilTest.signOutProcess(auth0, 200);
    signInResp = utilTest.tokenProcess(SIGN_IN, auth0, 200);
    minTokenEnhanceDataCheck(signInResp);
    data = (Map<String, Object>) signInResp.get(dataKey);
    assertNull(avatarUrlValue);
    assertNull(data.get(avatarUrlKey));

    var url = "avatar.png";
    var access1 = getAccessByUsername(Account2_ID, NevisAccountUsernameType.ID);
    assertNull(access1.getAvatarUrl());
    utilTest.check(nevisApiRoot() + resource, POST, auth.getUSER().access_token, checkers_for_setAvatarUrl2(url));
    access1 = getAccessByUsername(Account2_EMAIL, NevisAccountUsernameType.EMAIL);
    avatarUrlValue = access1.getAvatarUrl();
    utilTest.signOutProcess(auth.getUSER(), 200);
    signInResp = utilTest.tokenProcess(SIGN_IN, auth.getUSER(), 200);
    minTokenEnhanceDataCheck(signInResp);
    data = (Map<String, Object>) signInResp.get(dataKey);
    assertEquals(url, avatarUrlValue);
    assertEquals(url, data.get(avatarUrlKey));


//    var signInResp = utilTest.tokenProcess(SIGN_IN, auth.of(Account2_EMAIL, null, Account2_Pass, clientTrusted), 200);
//    var tokenRefreshResp = utilTest.tokenProcess(REFRESH, auth.of(Account1_EMAIL, EMAIL, Account2_Pass, clientTrusted), 200);
  }


  //-------------------------------------------------------
  // Account.Email
  //

  @Test
  public void _04_01_getAccountEmail()
  {
    var resource = propNevis.getResource().getGetAccountEmail();
    var auth0 = auth.of(USER, Account4_NICKNAME, NICKNAME, Account4_Pass, client.getClientTrusted());

    utilTest.check(nevisApiRoot() + resource, GET, auth0.access_token, checkers_for_getAccountEmail1);
    utilTest.check(nevisApiRoot() + resource, GET, auth.getADMIN().access_token, checkers_for_getAccountEmail2);
  }

  @Test
  public void _04_02_emailConfirmReq()
  {
    var timeToWait = TimeUnit.MILLISECONDS.toSeconds(propNevis.getScheduledTaskMailing().getCollectFromDbInterval()) * 2;
    var resource = propNevis.getResource().getEmailConfirmReq();
    var clientTrusted = client.getClientTrusted();
    var type = EMAIL_CONFIRM;

    mailingService.deleteAll();

    // (0) check error 'no-email-associated-with-account'
    var auth0 = auth.of(USER, Account4_NICKNAME, NICKNAME, Account4_Pass, clientTrusted);
    utilTest.check(nevisApiRoot() + resource, GET, auth0.access_token, checkers_for_emailConfirmReq_noEmailAssociatedWithAccount);

    // (1) check error 'email-is-already-confirmed'
    var auth_USER = auth.getUSER();
    var aEmail = getAccountEmailByEmail(Account2_EMAIL);
    assertTrue(aEmail.isConfirmed());
    utilTest.check(nevisApiRoot() + resource, GET, auth_USER.access_token, checkers_for_emailConfirmReq_isAlreadyConfirmed);

    // (2) successful confirmation request
    var email = Account6_EMAIL;
    var auth1 = auth.of(USER, Account6_EMAIL, EMAIL, Account6_Pass, clientTrusted);
    var accessToken = auth1.access_token;
    aEmail = getAccountEmailByEmail(email);
    assertFalse(aEmail.isConfirmed());

    var mailingList = getMailingListByTypeAndEmail(type, email);
    assertEquals(0, mailingList.size());
    utilTest.check(nevisApiRoot() + resource, GET, accessToken, checkers_for_emailConfirmReq);

    var mailing = getMailingFirstOfOne(type, email);
    assertFalse(mailing.isScheduled());
    assertFalse(mailing.isMaxAttemptsReached());
    assertTrue(mailing.getData().length() >= 35);

    // (3)
    // Ok. At the moment we have 1 key and it is not yet time to send a duplicate request
    // Let's try to add one more key ==> сheck for 'delay-between-duplicate-requests' error
    utilTest.check(nevisApiRoot() + resource, GET, accessToken, checkers_for_emailConfirmReq_duplicateDelay);

    pleaseWait(timeToWait, log);
    mailing = getMailingLastScheduledNotEmptyData(type, email);
    assertFalse(mailing.isMaxAttemptsReached());
    var alreadyScheduledKeyOld = mailing.getData();

    // (4)
    // At the moment we have a key that has already been scheduled and is waiting for confirmation
    // Try to add one more key, because duplicate delay should already expire
    utilTest.check(nevisApiRoot() + resource, GET, accessToken, checkers_for_emailConfirmReq);
    utilTest.check(nevisApiRoot() + resource, GET, accessToken, checkers_for_emailConfirmReq_duplicateDelay);

    pleaseWait(timeToWait, log);
    mailing = getMailingLastScheduledNotEmptyData(type, email);
    assertFalse(mailing.isMaxAttemptsReached());
    assertNotEquals(alreadyScheduledKeyOld, mailing.getData());  // another new key was success added and scheduled

    aEmail = getAccountEmailByEmail(email);
    assertFalse(aEmail.isConfirmed());
  }

  @Test
  public void _04_03_emailConfirm()
  {
    var type = EMAIL_CONFIRM;
    var email = Account6_EMAIL;
    var resource = propNevis.getResource().getEmailConfirm();

    var aEmail = getAccountEmailByEmail(email);
    assertFalse(aEmail.isConfirmed());

    var mailingList = getMailingScheduledNotEmptyData(type, email);
    assertEquals(2, mailingList.size());
    var confirmKey = mailingList.get(0).getData();
    utilTest.check(nevisApiRoot() + resource, POST, null, checkers_for_confirmEmail(confirmKey));

    mailingList = getMailingScheduledNotEmptyData(type, email);
    assertEquals(1, mailingList.size());

    aEmail = getAccountEmailByEmail(email);
    assertTrue(aEmail.isConfirmed());
  }

  @Test
  public void _04_04_emailChange()
  {
    var clientTrusted = client.getClientTrusted();
    var resource = propNevis.getResource().getEmailChange();

    var auth1 = auth.of(Account5_EMAIL, EMAIL, Account5_Pass, clientTrusted);
    utilTest.tokenProcess(SIGN_IN, auth1, 400);
    auth1 = auth.of(USER, Account5_PHONE, PHONE, Account5_Pass, clientTrusted);

    var auth2 = auth.of(Account6_NewEmail, EMAIL, Account6_Pass, clientTrusted);
    utilTest.tokenProcess(SIGN_IN, auth2, 400);
    auth2.username = Account6_EMAIL;
    utilTest.tokenProcess(SIGN_IN, auth2, 200);
    var aEmail = getAccountEmailByEmail(auth2.username);
    checkAccountEmail(aEmail, auth2.username, true, true);

    utilTest.check(nevisApiRoot() + resource, POST, auth1.access_token, checkers_for_emailChange1);
    utilTest.check(nevisApiRoot() + resource, POST, auth2.access_token, checkers_for_emailChange2);

    auth1 = auth.of(USER, Account5_PHONE, PHONE, Account5_Pass, clientTrusted);
    auth1 = auth.of(USER, Account5_EMAIL, EMAIL, Account5_Pass, clientTrusted);
    aEmail = getAccountEmailByEmail(auth1.username);
    checkAccountEmail(aEmail, auth1.username, true, false);

    utilTest.tokenProcess(SIGN_IN, auth2, 400);
    auth2.username = Account6_NewEmail;
    utilTest.tokenProcess(SIGN_IN, auth2, 200);
    aEmail = getAccountEmailByEmail(auth2.username);
    checkAccountEmail(aEmail, auth2.username, true, false);
  }

  @Test
  public void _04_05_updateAccountEmail()
  {
    var clientTrusted = client.getClientTrusted();
    var resource = propNevis.getResource().getUpdateAccountEmail();
    var auth1 = auth.of(USER, Account4_NICKNAME, NICKNAME, Account4_Pass, clientTrusted);
    var auth2 = auth.of(USER, Account5_PHONE, PHONE, Account5_Pass, clientTrusted);
    var aEmail = getAccountEmailByEmail(Account5_EMAIL);

    checkAccountEmail(aEmail, Account5_EMAIL, true, false);

    utilTest.check(nevisApiRoot() + resource, POST, auth1.access_token, checkers_for_updateAccountEmail1);
    utilTest.check(nevisApiRoot() + resource, POST, auth2.access_token, checkers_for_updateAccountEmail2);

    aEmail = getAccountEmailByEmail(Account5_EMAIL);
    checkAccountEmail(aEmail, Account5_EMAIL, false, false);
  }


  //-------------------------------------------------------
  // Account.Phone
  //

  @Test
  public void _05_01_getAccountPhone()
  {
    var resource = propNevis.getResource().getGetAccountPhone();
    var clientTrusted = client.getClientTrusted();
    var auth1 = auth.of(USER, Account4_NICKNAME, NICKNAME, Account4_Pass, clientTrusted);
    var auth2 = auth.of(USER, Account5_PHONE, PHONE, Account5_Pass, clientTrusted);

    utilTest.check(nevisApiRoot() + resource, GET, auth1.access_token, checkers_for_getAccountPhone1);
    utilTest.check(nevisApiRoot() + resource, GET, auth2.access_token, checkers_for_getAccountPhone2);
  }

  @Test
  public void _05_02_phoneChange()
  {
    var clientTrusted = client.getClientTrusted();
    var resource = propNevis.getResource().getPhoneChange();
    var auth1 = auth.of(Account7_PHONE, PHONE, Account7_Pass_Decoded, clientTrusted);

    utilTest.tokenProcess(SIGN_IN, auth1, 400);
    auth1 = auth.of(USER, Account7_EMAIL, EMAIL, Account7_Pass_Decoded, clientTrusted);

    var auth2 = auth.of(Account5_NewPhone, PHONE, Account5_Pass, clientTrusted);
    utilTest.tokenProcess(SIGN_IN, auth2, 400);
    auth2.username = Account5_PHONE;
    utilTest.tokenProcess(SIGN_IN, auth2, 200);
    var aPhone = getAccountPhoneByPhone(Account5_PHONE, true);
    checkAccountPhone(aPhone, Account5_PHONE, true, false);

    utilTest.check(nevisApiRoot() + resource, POST, auth1.access_token, checkers_for_phoneChange1);
    utilTest.check(nevisApiRoot() + resource, POST, auth2.access_token, checkers_for_phoneChange2);

    auth1 = auth.of(USER, Account7_PHONE, PHONE, Account7_Pass_Decoded, clientTrusted);
    aPhone = getAccountPhoneByPhone(Account7_PHONE, true);
    checkAccountPhone(aPhone, Account7_PHONE, true, false);

    utilTest.tokenProcess(SIGN_IN, auth2, 400);
    auth2.username = Account5_NewPhone;
    utilTest.tokenProcess(SIGN_IN, auth2, 200);
    aPhone = getAccountPhoneByPhone(Account5_NewPhone, true);
    checkAccountPhone(aPhone, Account5_NewPhone, true, false);
  }

  @Test
  public void _05_03_updateAccountPhone()
  {
    var clientTrusted = client.getClientTrusted();
    var resource = propNevis.getResource().getUpdateAccountPhone();
    var auth1 = auth.of(USER, Account3_EMAIL, EMAIL, Account3_Pass, clientTrusted);
    var auth2 = auth.of(USER, Account5_NewPhone, PHONE, Account5_Pass, clientTrusted);
    var aPhone = getAccountPhoneByPhone(Account5_NewPhone, true);

    checkAccountPhone(aPhone, Account5_NewPhone, true, false);

    utilTest.check(nevisApiRoot() + resource, POST, auth1.access_token, checkers_for_updateAccountPhone1);
    utilTest.check(nevisApiRoot() + resource, POST, auth2.access_token, checkers_for_updateAccountPhone2);

    aPhone = getAccountPhoneByPhone(Account5_NewPhone, true);
    checkAccountPhone(aPhone, Account5_NewPhone, false, false);
  }


  //-------------------------------------------------------
  // Account.Personal
  //

  @Test
  public void _06_01_getAccountPersonal()
  {
    var resource = propNevis.getResource().getGetAccountPersonal();
    var clientTrusted = client.getClientTrusted();
    var auth1 = auth.of(USER, Account6_NewEmail, EMAIL, Account6_Pass, clientTrusted);
    var auth2 = auth.of(USER, Account7_EMAIL, EMAIL, Account7_Pass_Decoded, clientTrusted);
    var auth3 = auth.of(USER, Account4_NICKNAME, NICKNAME, Account4_Pass, clientTrusted);

    utilTest.check(nevisApiRoot() + resource, GET, auth1.access_token, checkers_for_getAccountPersonal1);
    utilTest.check(nevisApiRoot() + resource, GET, auth2.access_token, checkers_for_getAccountPersonal2);
    utilTest.check(nevisApiRoot() + resource, GET, auth3.access_token, checkers_for_getAccountPersonal3);
  }

  @Test
  public void _06_02_nicknameChange()
  {
    var clientTrusted = client.getClientTrusted();
    var resource = propNevis.getResource().getNicknameChange();
    var auth1 = auth.of(Account4_NewNickName, NICKNAME, Account4_Pass, clientTrusted);

    utilTest.tokenProcess(SIGN_IN, auth1, 400);
    auth1.username = Account4_NICKNAME;
    utilTest.tokenProcess(SIGN_IN, auth1, 200);

    var auth2 = auth.of(Account7_NICKNAME, NICKNAME, Account7_Pass_Decoded, clientTrusted);
    utilTest.tokenProcess(SIGN_IN, auth2, 400);
    auth2 = auth.of(USER, Account7_EMAIL, EMAIL, Account7_Pass_Decoded, clientTrusted);

    utilTest.check(nevisApiRoot() + resource, POST, auth1.access_token, checkers_for_nicknameChange1);
    utilTest.check(nevisApiRoot() + resource, POST, auth2.access_token, checkers_for_nicknameChange2);

    utilTest.tokenProcess(SIGN_IN, auth1, 400);
    auth1.username = Account4_NewNickName;
    utilTest.tokenProcess(SIGN_IN, auth1, 200);
    checkAccountPersonal_ExactMatch(Account4_ID, NevisAccountPersonal.of(
            Account4_NewNickName, true,           // nickName
            null, true,                           // firstName
            null, true,                           // middleName
            null, true,                           // lastName
            M, true,                              // gender
            LocalDate.parse("1980-11-27"), true,  // dateOfBirth
            "DE", true,                           // country
            null, true,                           // city
            null, true,                           // company
            null, true)                           // positionHeld
    );

    auth2 = auth.of(USER, Account7_NICKNAME, NICKNAME, Account7_Pass_Decoded, clientTrusted);
    checkAccountPersonal_ExactMatch(Account7_ID, NevisAccountPersonal.of(
            Account7_NICKNAME, true,      // nickName
            "12345678901234567890", true, // firstName
            "12345678901234567890", true, // middleName
            "12345678901234567890", true, // lastName
            null, true,                   // gender
            null, true,                   // dateOfBirth
            null, true,                   // country
            "1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890", true, // city
            "1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890", true, // company
            "1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890", true) // positionHeld
    );
  }

  @Test
  public void _06_03_updateAccountPersonal()
  {
    var clientTrusted = client.getClientTrusted();
    var resource = propNevis.getResource().getUpdateAccountPersonal();
    var auth1 = auth.of(USER, Account6_NewEmail, EMAIL, Account6_Pass, clientTrusted);

    utilTest.check(nevisApiRoot() + resource, POST, auth1.access_token, checkers_for_updateAccountPersonal1);
    checkAccountPersonal_ExactMatch(Account6_ID, NevisAccountPersonal.of(
            null, false,                           // nickName
            "12345678901234567890", false,         // firstName
            "12345678901234567890", false,         // middleName
            "12345678901234567890", false,         // lastName
            M, false,                              // gender
            LocalDate.parse("2018-07-11"), false,  // dateOfBirth
            "RU", false,                           // country
            "1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890", false, // city
            "1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890", false, // company
            "1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890", false) // positionHeld
    );

    utilTest.check(nevisApiRoot() + resource, POST, auth1.access_token, checkers_for_updateAccountPersonal2);
    checkAccountPersonal_ExactMatch(Account6_ID, NevisAccountPersonal.of(
            null, true, // nickName
            null, true, // firstName
            null, true, // middleName
            null, true, // lastName
            null, true, // gender
            null, true, // dateOfBirth
            null, true, // country
            null, true, // city
            null, true, // company
            null, true) // positionHeld
    );
  }


  //-------------------------------------------------------
  // UTILs
  //

  private String nevisApiRoot()
  {
    return propDwfe.getService().getGatewayUrl() + propDwfe.getService().getNevis().getGatewayPath();
  }

  private void minTokenEnhanceDataCheck(Map<String, Object> map)
  {
    var dataKey = "data";
    // TokenEnhance fields:
    var idKey = "id";
    var usernameKey = "username";
    var authoritiesKey = "authorities";
    var thirdPartyKey = "thirdParty";
    var avatarUrlKey = "avatarUrl";
    var createdOnKey = "createdOn";

    assertTrue(map.containsKey(dataKey));
    var data = (Map<String, Object>) map.get(dataKey);

    assertTrue(data.containsKey(idKey));
    assertTrue(data.containsKey(usernameKey));
    assertTrue(data.containsKey(authoritiesKey));
    assertTrue(data.containsKey(thirdPartyKey));
    assertTrue(data.containsKey(avatarUrlKey));
    assertTrue(data.containsKey(createdOnKey));

    assertNotNull(data.get(idKey));
    assertNotNull(data.get(usernameKey));
    assertNotNull(data.get(authoritiesKey));
    assertNotNull(data.get(createdOnKey));
  }

  private void fullAuth(DwfeTestAuth auth)
  {
    utilTest.fullAuthProcess(auth);
    mailingService.deleteAll();
  }

  private NevisAccountAccess checkAccountAccessAfterCreateAccount(String username, DwfeTestAccountUsernameType usernameType, NevisAccountThirdParty thirdParty)
  {
    var aAccess = getAccessByUsername(username, dwfeUsernameTypeToNevis(usernameType));

    assertTrue(aAccess.getId() > 999);
    assertEquals(username, aAccess.getSignedInUsername());
    assertEquals(dwfeUsernameTypeToNevis(usernameType), aAccess.getUsernameType());
    if (EMAIL == usernameType)
      assertTrue(isEmailValid(aAccess.getSignedInUsername(), "email", new ArrayList<>()));
    else if (NICKNAME == usernameType)
    {
      // ???
    }
    else if (PHONE == usernameType)
    {
      // ???
    }
    else if (ID == usernameType)
      assertTrue(isIdValid(aAccess.getSignedInUsername(), new ArrayList<>()));

    assertTrue(isStringBcrypted(aAccess.getPassword().replace("{bcrypt}", "")));

    assertNotEquals(null, aAccess.getCreatedOn());
    assertNotEquals(null, aAccess.getUpdatedOn());

    var authorities = aAccess.getAuthorities();
    assertEquals(3, authorities.size());
    assertEquals(authorities, getListOfInitAuthorities());
    assertEquals(thirdParty, aAccess.getThirdParty());

    assertTrue(aAccess.isAccountNonExpired());
    assertTrue(aAccess.isCredentialsNonExpired());
    assertTrue(aAccess.isAccountNonLocked());
    assertTrue(aAccess.isEnabled());

    return aAccess;
  }

  private NevisAccountAccess getAccessByUsername(String username, NevisAccountUsernameType usernameType)
  {
    var aAccessOpt = accessService.findByUsername(username, usernameType);
    assertTrue(aAccessOpt.isPresent());
    return aAccessOpt.get();
  }

  private void checkAccountEmail(NevisAccountEmail aEmail, String expectedValue, boolean expectedNonPublic, boolean expectedConfirmed)
  {
    var email = aEmail.getValue();
    assertEquals(expectedValue, email);
    assertTrue(isEmailValid(email, "email", List.of()));
    assertEquals(expectedConfirmed, aEmail.isConfirmed());
    assertEquals(expectedNonPublic, aEmail.isNonPublic());
  }

  private NevisAccountEmail getAccountEmailById(Long id, boolean expectedIsPresent)
  {
    var aEmailOpt = emailService.findById(id);
    assertEquals(expectedIsPresent, aEmailOpt.isPresent());
    return expectedIsPresent ? aEmailOpt.get() : null;
  }

  private NevisAccountEmail getAccountEmailByEmail(String email)
  {
    var aEmailOpt = emailService.findByValue(email);
    assertTrue(aEmailOpt.isPresent());
    return aEmailOpt.get();
  }

  private void checkAccountPhone(NevisAccountPhone aPhone, String expectedValue, boolean expectedNonPublic, boolean expectedConfirmed)
  {
    var phone = aPhone.getValue();
    assertEquals(expectedValue, phone);
    assertEquals(expectedConfirmed, aPhone.isConfirmed());
    assertEquals(expectedNonPublic, aPhone.isNonPublic());
  }

  private NevisAccountPhone getAccountPhoneById(Long id, boolean expectedIsPresent)
  {
    var aPhoneOpt = phoneService.findById(id);
    assertEquals(expectedIsPresent, aPhoneOpt.isPresent());
    return expectedIsPresent ? aPhoneOpt.get() : null;
  }

  private NevisAccountPhone getAccountPhoneByPhone(String phone, boolean expectedIsPresent)
  {
    var aPhoneOpt = phoneService.findByValue(phone);
    assertEquals(expectedIsPresent, aPhoneOpt.isPresent());
    return expectedIsPresent ? aPhoneOpt.get() : null;
  }

  private void checkAccountPersonal_ExactMatch(Long id, NevisAccountPersonal tPersonal)
  {
    var aPersonal = getAccountPersonalById(id);
    assertNotEquals(null, aPersonal.getUpdatedOn());

    var tNickName = tPersonal.getNickName();
    var tNickNameNonPublic = tPersonal.getNickNameNonPublic();

    var tFirstName = tPersonal.getFirstName();
    var tFirstNameNonPublic = tPersonal.getFirstNameNonPublic();

    var tMiddleName = tPersonal.getMiddleName();
    var tMiddleNameNonPublic = tPersonal.getMiddleNameNonPublic();

    var tLastName = tPersonal.getLastName();
    var tLastNameNonPublic = tPersonal.getLastNameNonPublic();

    var tGender = tPersonal.getGender();
    var tGenderNonPublic = tPersonal.getGenderNonPublic();

    var tDateOfBirth = tPersonal.getDateOfBirth();
    var tDateOfBirthNonPublic = tPersonal.getDateOfBirthNonPublic();

    var tCountry = tPersonal.getCountry();
    var tCountryNonPublic = tPersonal.getCountryNonPublic();

    var tCity = tPersonal.getCity();
    var tCityNonPublic = tPersonal.getCityNonPublic();

    var tCompany = tPersonal.getCompany();
    var tCompanyNonPublic = tPersonal.getCompanyNonPublic();

    var tPositionHeld = tPersonal.getPositionHeld();
    var tPositionHeldNonPublic = tPersonal.getPositionHeldNonPublic();

    assertEquals(tNickName, aPersonal.getNickName());
    assertEquals(tNickNameNonPublic, aPersonal.getNickNameNonPublic());

    assertEquals(tFirstName, aPersonal.getFirstName());
    assertEquals(tFirstNameNonPublic, aPersonal.getFirstNameNonPublic());

    assertEquals(tMiddleName, aPersonal.getMiddleName());
    assertEquals(tMiddleNameNonPublic, aPersonal.getMiddleNameNonPublic());

    assertEquals(tLastName, aPersonal.getLastName());
    assertEquals(tLastNameNonPublic, aPersonal.getLastNameNonPublic());

    assertEquals(tGender, aPersonal.getGender());
    assertEquals(tGenderNonPublic, aPersonal.getGenderNonPublic());

    assertEquals(tDateOfBirth, aPersonal.getDateOfBirth());
    assertEquals(tDateOfBirthNonPublic, aPersonal.getDateOfBirthNonPublic());

    assertEquals(tCountry, aPersonal.getCountry());
    assertEquals(tCountryNonPublic, aPersonal.getCountryNonPublic());

    assertEquals(tCity, aPersonal.getCity());
    assertEquals(tCityNonPublic, aPersonal.getCityNonPublic());

    assertEquals(tCompany, aPersonal.getCompany());
    assertEquals(tCompanyNonPublic, aPersonal.getCompanyNonPublic());

    assertEquals(tPositionHeld, aPersonal.getPositionHeld());
    assertEquals(tPositionHeldNonPublic, aPersonal.getPositionHeldNonPublic());
  }

  private NevisAccountPersonal getAccountPersonalById(Long id)
  {
    var aPersonalOpt = personalService.findById(id);
    assertTrue(aPersonalOpt.isPresent());
    return aPersonalOpt.get();
  }

  private List<NevisMailing> getMailingListByTypeAndEmail(NevisMailingType type, String email)
  {
    return mailingService.findByTypeAndEmail(type, email);
  }

  private NevisMailing getMailingLastScheduledNotEmptyData(NevisMailingType type, String email)
  {
    var mailingOpt = mailingService.findLastScheduledNotEmptyData(type, email);
    assertTrue(mailingOpt.isPresent());
    return mailingOpt.get();
  }

  private List<NevisMailing> getMailingScheduledNotEmptyData(NevisMailingType type, String email)
  {
    return mailingService.findScheduledNotEmptyData(type, email);
  }

  private NevisMailing getMailingFirstOfOne(NevisMailingType type, String email)
  {
    var mailing = getMailingListByTypeAndEmail(type, email);
    assertEquals(1, mailing.size());
    return mailing.get(0);
  }

  private DwfeTestAccountUsernameType nevisUsernameTypeToDwfe(NevisAccountUsernameType nevisAccountUsernameType)
  {
    return DwfeTestAccountUsernameType.valueOf(nevisAccountUsernameType.toString());
  }

  private NevisAccountUsernameType dwfeUsernameTypeToNevis(DwfeTestAccountUsernameType dwfeTestAccountUsernameType)
  {
    return NevisAccountUsernameType.valueOf(dwfeTestAccountUsernameType.toString());
  }
}
