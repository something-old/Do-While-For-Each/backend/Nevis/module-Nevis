package dwfe.modules.nevis;

import dwfe.config.DwfeConfigProperties;
import dwfe.modules.nevis.config.NevisConfigProperties;
import dwfe.test.config.DwfeTestConfigProperties;
import dwfe.test.config.DwfeTestLevelAuthority;
import dwfe.test.config.DwfeTestVariablesForAuthAccessTest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.HashMap;
import java.util.Map;

import static dwfe.test.config.DwfeTestLevelAuthority.ANY;
import static dwfe.test.config.DwfeTestLevelAuthority.USER;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@Component
public class NevisTestVariablesForAuthAccessTest extends DwfeTestVariablesForAuthAccessTest
{
  @Autowired
  private DwfeConfigProperties propDwfe;
  @Autowired
  private DwfeTestConfigProperties propDwfeTest;
  @Autowired
  private NevisConfigProperties propNevis;

  //-------------------------------------------------------
  // RESOURCES
  //

  public Map<String, Map<DwfeTestLevelAuthority, Map<RequestMethod, Map<String, Object>>>> RESOURCE_AUTHORITY_reqDATA()
  {
    var nevisUri = propDwfe.getService().getGatewayUrl() + propDwfe.getService().getNevis().getGatewayPath();
    Map<String, Map<DwfeTestLevelAuthority, Map<RequestMethod, Map<String, Object>>>> result = new HashMap<>();

    // Account.Common
    result.put(nevisUri + propNevis.getResource().getCanUseUsername(), Map.of(ANY, Map.of(POST, Map.of())));
    result.put(nevisUri + propNevis.getResource().getCanUsePassword(), Map.of(ANY, Map.of(POST, Map.of())));
    result.put(nevisUri + propNevis.getResource().getCreateAccount(), Map.of(ANY, Map.of(POST, Map.of())));

    // Account.Access
    result.put(nevisUri + propNevis.getResource().getGetAccountAccess(), Map.of(USER, Map.of(GET, Map.of())));
    result.put(nevisUri + propNevis.getResource().getPasswordChange(), Map.of(USER, Map.of(POST, Map.of())));
    result.put(nevisUri + propNevis.getResource().getPasswordResetReq(), Map.of(ANY, Map.of(POST, Map.of())));
    result.put(nevisUri + propNevis.getResource().getPasswordReset(), Map.of(ANY, Map.of(POST, Map.of())));
    result.put(nevisUri + propNevis.getResource().getSetAvatarUrl(), Map.of(USER, Map.of(POST, Map.of())));

    // Account.Email
    result.put(nevisUri + propNevis.getResource().getGetAccountEmail(), Map.of(USER, Map.of(GET, Map.of())));
    result.put(nevisUri + propNevis.getResource().getEmailConfirmReq(), Map.of(USER, Map.of(GET, Map.of())));
    result.put(nevisUri + propNevis.getResource().getEmailConfirm(), Map.of(ANY, Map.of(POST, Map.of())));
    result.put(nevisUri + propNevis.getResource().getEmailChange(), Map.of(USER, Map.of(POST, Map.of())));
    result.put(nevisUri + propNevis.getResource().getUpdateAccountEmail(), Map.of(USER, Map.of(POST, Map.of())));

    // Account.Phone
    result.put(nevisUri + propNevis.getResource().getGetAccountPhone(), Map.of(USER, Map.of(GET, Map.of())));
    result.put(nevisUri + propNevis.getResource().getPhoneChange(), Map.of(USER, Map.of(POST, Map.of())));
    result.put(nevisUri + propNevis.getResource().getUpdateAccountPhone(), Map.of(USER, Map.of(POST, Map.of())));

    // Account.Personal
    result.put(nevisUri + propNevis.getResource().getGetAccountPersonal(), Map.of(USER, Map.of(GET, Map.of())));
    result.put(nevisUri + propNevis.getResource().getNicknameChange(), Map.of(USER, Map.of(POST, Map.of())));
    result.put(nevisUri + propNevis.getResource().getUpdateAccountPersonal(), Map.of(USER, Map.of(POST, Map.of())));

    return result;
  }
}
